package in.co.laundryday.laundryday.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import in.co.laundryday.laundryday.R;

public class RegisterActivity extends BaseActivity {

    private Activity activity = RegisterActivity.this;

    private Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Register");
        getSupportActionBar().setElevation(0);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//        ConstantsFuns.startWindowAnimations(activity);
        btnRegister = findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(activity, DashboardActivity.class));
            }
        });
    }


}
