package in.co.laundryday.laundryday.Utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONObject;

import in.co.laundryday.laundryday.Activities.LoginActivity;

public class SessionManager {
    private static final String SP_MAIN_PROFILE = "SP_MAIN_PROFILE";
    private static final String SP_PROFILE = "SP_MAIN_PROFILE";
    public static final String SP_USER_PROFILE_DATA = "SP_USER_PROFILE_DATA";
    private static final String SP_USER_LOGGED_IN = "SP_USER_LOGGED_IN";
    private static final String SP_USER_PROFILE_ROLE = "SP_USER_PROFILE_ROLE";
    private SharedPreferences.Editor editor;
    private SharedPreferences preferences;
    private Context context;

    public SessionManager(Context context) {
        this.context = context;
        editor = context.getSharedPreferences(SP_MAIN_PROFILE, Context.MODE_PRIVATE).edit();
        preferences = context.getSharedPreferences(SP_MAIN_PROFILE, Context.MODE_PRIVATE);
        editor.apply();
    }

    // Student Login give Only user data object

    public void CreateLoginStudent(JSONObject userData, String userRole) {

        Log.d("JSON_DATA", "CreateLoginStudent: " + userData.toString());

        editor.putString(SP_USER_PROFILE_DATA, userData.toString());
        editor.putString(SP_USER_PROFILE_ROLE, userRole);
        editor.putBoolean(SP_USER_LOGGED_IN, true);
        editor.apply();
    }

    public String onCheckLogin() {
        String userRole = "";
        if (!this.isLoggedIn()) {
            context.startActivity(new Intent(context, LoginActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));

        } else {
            userRole = preferences.getString(SP_USER_PROFILE_ROLE, "");
        }
        return userRole;
    }

    private boolean isLoggedIn() {
        return preferences.getBoolean(SP_USER_LOGGED_IN, false);

    }

    public String getUserProfile() {
        return (preferences.getString(SP_USER_PROFILE_DATA, null));
    }


    public void onLogoutUser() {
        editor.clear();
        editor.apply();
        onCheckLogin();
    }
}
