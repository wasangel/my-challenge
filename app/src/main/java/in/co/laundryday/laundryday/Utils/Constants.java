package in.co.laundryday.laundryday.Utils;

public class Constants {
    public static final String USER_API = "https://kukadifresh.in/laundryday/AndroidApp/readapi.php";

    public static final String PARAMETER_ACTION = "action";
    public static final String PARAMETER_NEW_REGISTRAION = "newRegister";
    public static final String PARAMETER_SEND_OTP = "sendOtp";
    public static final String PARAMETER_CHECK_USER = "checkUser";

    public static final String PARAMETER_CUSTOMER_NAME = "customer_name";
    public static final String PARAMETER_CUSTOMER_EMAIL = "email";
    public static final String PARAMETER_CUSTOMER_MOBILE = "mobile";
    public static final String PARAMETER_CUSTOMER_PASSWORD = "password";
    public static final String PARAMETER_CUSTOMER_USER_TOKEN = "user_token";

    public static final String API_RESPONSE_STATUS = "responseStatus";
    public static final String API_MESSAGE = "message";
    public static final String PARAM_USER_LOGIN = "userLogin";
    public static final String OTP_CODE = "code";
    public static final String INTENT_OTP_KEY = "INTENT_OTP_KEY";
    public static final String INTENT_MOBILE_KEY = "INTENT_MOBILE_KEY";
}
