package in.co.laundryday.laundryday.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;

import in.co.laundryday.laundryday.R;
import in.co.laundryday.laundryday.Utils.Constants;
import in.co.laundryday.laundryday.Utils.ConstantsFuns;

public class VerifyActivity extends BaseActivity {

    private Button btnVerify;
    private TextView txtResendTimer;
    private Activity activity = VerifyActivity.this;
    private long maxTimeInMilliseconds = 30000;// in your case
    ProgressDialog progressDialog;
    private PinView firstPinView;
    private String enteredOtp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_verify);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        // ConstantsFuns.startWindowAnimations(activity);
        btnVerify = findViewById(R.id.btnVerify);
        firstPinView = findViewById(R.id.firstPinView);
        txtResendTimer = findViewById(R.id.txtResendTimer);

        Intent intent = getIntent();
        final int otpCode = intent.getIntExtra(Constants.INTENT_OTP_KEY, 0);

        String mobileNumber = intent.getStringExtra(Constants.INTENT_MOBILE_KEY);

        // progressDialog = ConstantsFuns.onShowProgressDialog(activity);
        btnVerify.setVisibility(View.GONE);
        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                enteredOtp = firstPinView.getText().toString().trim();
               // Toast.makeText(activity, enteredOtp, Toast.LENGTH_SHORT).show();

                int verifyOtp = Integer.parseInt(enteredOtp);

                if (verifyOtp == 1234) {
                    startActivity(new Intent(activity, RegisterActivity.class));
                }


            }
        });

        firstPinView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (firstPinView.getText().toString().length() == 4) {
                    //Toast.makeText(activity, "Verified", Toast.LENGTH_SHORT).show();
                    btnVerify.setVisibility(View.VISIBLE);
                    ConstantsFuns.hideKeyBoard(activity);
                    enteredOtp = firstPinView.getText().toString().trim();
                    // Toast.makeText(activity, enteredOtp, Toast.LENGTH_SHORT).show();

                    int verifyOtp = Integer.parseInt(enteredOtp);

                    if (verifyOtp == 1234) {
                        startActivity(new Intent(activity, RegisterActivity.class));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        startTimer(maxTimeInMilliseconds, 1000);
    }


    public void startTimer(final long finish, long tick) {
        CountDownTimer t;
        t = new CountDownTimer(finish, tick) {

            public void onTick(long millisUntilFinished) {
                long remainedSecs = millisUntilFinished / 1000;
                txtResendTimer.setText("Resend in  " + (remainedSecs / 60) + ":" + (remainedSecs % 60));// manage it accordign to you
            }

            public void onFinish() {
                txtResendTimer.setText("Resend ?");
                txtResendTimer.setClickable(true);
                txtResendTimer.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_resend, 0);
                // progressDialog.dismiss();
                //  Toast.makeText(activity, "Finish", Toast.LENGTH_SHORT).show();

                cancel();
            }
        }.start();
    }
}
