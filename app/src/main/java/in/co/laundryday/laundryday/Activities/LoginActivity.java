package in.co.laundryday.laundryday.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.co.laundryday.laundryday.R;
import in.co.laundryday.laundryday.Utils.AppController;
import in.co.laundryday.laundryday.Utils.Constants;
import in.co.laundryday.laundryday.Utils.ConstantsFuns;

public class LoginActivity extends BaseActivity {

    private Activity activity = LoginActivity.this;
    private Button btnLogin;
    private EditText etxLoginMoNumber, etxUserPassword;
    private ExpandableLayout expandPassword;
    private LinearLayout layoutProgress;
    private String userMoNumber, userPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        // ConstantsFuns.startWindowAnimations(activity);

        btnLogin = findViewById(R.id.btnLogin);
        etxLoginMoNumber = findViewById(R.id.etxLoginMoNumber);
        layoutProgress = findViewById(R.id.layoutProgress);
        expandPassword = findViewById(R.id.expandPassword);
        etxUserPassword = findViewById(R.id.etxUserPassword);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userMoNumber = etxLoginMoNumber.getText().toString().trim();
                userPassword = etxLoginMoNumber.getText().toString().trim();

                ConstantsFuns.hideKeyBoard(activity);
                // Toast.makeText(activity, userMoNumber, Toast.LENGTH_SHORT).show();
                onCheckUserLogin(userMoNumber, userPassword);
            }
        });


        etxLoginMoNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if (etxLoginMoNumber.getText().toString().trim().length() == 10) {

                    /// if user already registered then ask for password
                    ConstantsFuns.hideKeyBoard(activity);
                    checkUserIsAlreadyRegister(etxLoginMoNumber.getText().toString().trim());
                    // else then send otp and view verify otp and register user

                } else {

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etxUserPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    userMoNumber = etxLoginMoNumber.getText().toString().trim();
                    userPassword = etxLoginMoNumber.getText().toString().trim();

                    ConstantsFuns.hideKeyBoard(activity);
                    // Toast.makeText(activity, userMoNumber, Toast.LENGTH_SHORT).show();
                    onCheckUserLogin(userMoNumber, userPassword);
                    return true;
                }
                return false;
            }
        });


    }

    private void onCheckUserLogin(final String userMoNumber, final String userPassword) {

        layoutProgress.setVisibility(View.VISIBLE);
        StringRequest loginRequest = new StringRequest(Request.Method.POST, Constants.USER_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        layoutProgress.setVisibility(View.GONE);
                        try {
                            Log.d("USER_LOGIN_RES", "onResponse: " + response);

                            JSONObject jsonObject = new JSONObject(response);
                            boolean responseStatus = jsonObject.getBoolean(Constants.API_RESPONSE_STATUS);
                            String responseMessage = jsonObject.getString(Constants.API_MESSAGE);
                            //Toast.makeText(activity, responseMessage, Toast.LENGTH_SHORT).show();

                            startActivity(new Intent(activity, DashboardActivity.class));
                            // save user id

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(Constants.PARAMETER_ACTION, Constants.PARAM_USER_LOGIN);
                params.put(Constants.PARAMETER_CUSTOMER_MOBILE, userMoNumber);
                params.put(Constants.PARAMETER_CUSTOMER_PASSWORD, userPassword);
                params.put(Constants.PARAMETER_CUSTOMER_USER_TOKEN, "");
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(loginRequest);
    }

    private void checkUserIsAlreadyRegister(final String mobileNumber) {
        layoutProgress.setVisibility(View.VISIBLE);

        String urlAddress = Constants.USER_API;
        StringRequest checkMemberRequest = new StringRequest(Request.Method.POST, urlAddress,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        layoutProgress.setVisibility(View.GONE);
                        try {
                            Log.d("CHECK_USER_RES", "onResponse: " + response);
                            JSONObject jsonObject = new JSONObject(response);
                            boolean responseStatus = jsonObject.getBoolean(Constants.API_RESPONSE_STATUS);
                            String responseMessage = jsonObject.getString(Constants.API_MESSAGE);
                            // Toast.makeText(activity, responseMessage, Toast.LENGTH_SHORT).show();
                            if (responseStatus) {
                                expandPassword.expand();

                            } else {
                                sendConfirmOtpDialog(mobileNumber);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(Constants.PARAMETER_ACTION, Constants.PARAMETER_CHECK_USER);
                params.put(Constants.PARAMETER_CUSTOMER_MOBILE, mobileNumber);
                return params;
            }
        };

        Log.d("REQUEST_CHECK", "checkUserIsAlreadyRegister: " + checkMemberRequest.toString());

        AppController.getInstance().addToRequestQueue(checkMemberRequest);

    }

    private void sendConfirmOtpDialog(final String userMoNumber) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);

        // Setting Dialog Title
        alertDialog.setTitle("Confirm ?");

        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want send OTP to this mobile number (" + userMoNumber +
                ") ");

        // Setting Icon to Dialog


        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                // Write your code here to invoke YES event
                // Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();

                onSendOtp(userMoNumber);
                // send otp to user
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("Edit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                // Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    private void onSendOtp(final String userMoNumber) {

        StringRequest otpRequest = new StringRequest(Request.Method.POST, Constants.USER_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            Log.d("CHECK_USER_RES", "onResponse: " + response);
                            JSONObject jsonObject = new JSONObject(response);
                            boolean responseStatus = jsonObject.getBoolean(Constants.API_RESPONSE_STATUS);
                            String responseMessage = jsonObject.getString(Constants.API_MESSAGE);

                            if (responseStatus) {
                                int otpCode = jsonObject.getInt(Constants.OTP_CODE);

                                startActivity(new Intent(activity, VerifyActivity.class)
                                        .putExtra(Constants.INTENT_OTP_KEY, otpCode)
                                        .putExtra(Constants.INTENT_MOBILE_KEY, userMoNumber)
                                );

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(Constants.PARAMETER_ACTION, Constants.PARAMETER_SEND_OTP);
                params.put(Constants.PARAMETER_CUSTOMER_MOBILE, userMoNumber);
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(otpRequest);
    }

}
