package in.co.laundryday.laundryday.Utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.transition.Fade;
import android.transition.TransitionInflater;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;

import in.co.laundryday.laundryday.R;

public class ConstantsFuns {
    public static void startWindowAnimations(Activity context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Fade fade = (Fade) TransitionInflater.from(context).inflateTransition(R.transition.fade);
            context.getWindow().setExitTransition(fade);
        }
    }

    public static ProgressDialog onShowProgressDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        progressDialog.show();
        progressDialog.setIndeterminate(true);
        progressDialog.setContentView(R.layout.progresslayout);

        return progressDialog;
    }

    public static void hideKeyBoard(Activity context) {
        InputMethodManager inputManager = (InputMethodManager)
                context.getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(context.getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

}
